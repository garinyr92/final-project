<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/index', 'HomeController@index');

    // profil page
    Route::get('/profil', 'ProfilController@index');
    Route::get('/profils/{user_id}', 'ProfilController@friend_index');
    Route::get('/profil/info', 'ProfilController@info');
    Route::get('/profil/edit-password', 'ProfilController@edit_password');
    Route::post('/profil/save-password', 'ProfilController@save_password');
    Route::post('/profil/save-info', 'ProfilController@save_info');

    // friend
    Route::get('/friends', 'FriendsController@index');
    Route::get('/friend/add/{post_id}', 'FriendsController@add_friend');
    Route::get('/friend/delete/{post_id}', 'FriendsController@delete_friend');
    Route::post('/search', 'FriendsController@search_friend');

    Route::resource('post', 'PostController');
    // Route::delete('/post/{id}', 'PostController@destroy')->name('destroy');
    // Route::resource('comment', 'CommentController');
    Route::post('/comment/{post_id}', 'CommentController@store');

    // like post
    Route::get('like-post/{post_id}', 'LikeController@like');
    // Route::get('dislike-post/{post_id}', 'LikeController@dislike');

    // like comment
    Route::get('like-comment/{comment_id}', 'LikeController@likeComment');
    // Route::get('dislike-comment/{comment_id}', 'LikeController@dislikeComment');


});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
