<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeKomensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_komens', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('komentar_id');
            $table->foreign('komentar_id')->references('id')->on('komentars')->onDelete('cascade');

            // $table->unsignedBigInteger('komentar_user_id');
            // $table->foreign('komentar_user_id')->references('user_id')->on('komentars');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_komens');
    }
}
