@extends('layouts.master')

@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="new-postbox">
            {{-- <figure>
                <img src="{{ asset('asset/images/resources/admin2.jpg') }}" alt="">
            </figure> --}}
            <div class="newpst-input">
                <form method="post" action="{{ url('post')}}" enctype="multipart/form-data">
                    @csrf
                    <textarea rows="2" placeholder="write something" name="konten"></textarea>
                    <div class="attachments">
                        <ul>
                            <li>
                                <i class="fa fa-image"></i>
                                <label class="fileContainer">
                                    <input type="file" name="image">
                                </label>
                            </li>
                            <li>
                                <button type="submit">Post</button>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- add post new box -->
    <div class="loadMore">
        @foreach ($posts as $post)

        <div class="central-meta item">
            <div class="user-post">

                <div class="friend-info">
                    <div class="friend-name">
                        <ins><a href="{{ url('/profils', $post->username->id) }}" title="">{{ $post->username->name }}
                            </a></ins>
                        <span>published: {{ $post->created_at->format('j F, Y') }}</span>
                    </div>

                    @if($post->image == null)
                    <div class="post-meta">
                        <img src="#" alt="">
                        <div class="we-video-info">
                            <ul>
                                <li>
                                    <span class="comment" data-toggle="tooltip" title="Comments">
                                        <i class="fa fa-comments-o"></i>
                                        <ins>{{ $post->comments->count() }}</ins>
                                    </span>
                                </li>

                                <li>
                                    <a href="{{ url('/like-post', $post->id) }}">
                                        <span class="like" data-toggle="tooltip" title="like">
                                            <i class="ti-heart"></i>
                                            <ins>{{ $post->likes->count() }}</ins>
                                        </span>
                                    </a>
                                </li>
                                @if ($post->username->id == Auth::user()->id)

                                <li>
                                    <a href="/post/{{$post->id}}/edit">
                                        <span class="comment" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="/post/{{ $post->id }}"
                                        onclick="event.preventDefault(); document.getElementById('submit-form').submit();">
                                        @method('delete')
                                        <span class=" comment" data-toggle="tooltip" title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </a>
                                    <form id="submit-form" action="/post/{{ $post->id }}" method="POST" class="hidden">
                                        @csrf

                                        @method('DELETE')
                                    </form>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="description">

                            <p>
                                {{ $post->konten }}
                            </p>
                        </div>
                    </div>

                    @else

                    <div class="post-meta">
                        <img src="{{ URL::asset('storage/post/' . $post->image) }}" alt="" class="rounded">
                        <div class="we-video-info">
                            <ul>
                                <li>
                                    <span class="comment" data-toggle="tooltip" title="Comments">
                                        <i class="fa fa-comments-o"></i>
                                        <ins>{{ $post->comments->count() }}</ins>
                                    </span>
                                </li>

                                <li>
                                    <a href="{{ url('/like-post', $post->id) }}">
                                        <span class="like" data-toggle="tooltip" title="like">
                                            <i class="ti-heart"></i>
                                            <ins>{{ $post->likes->count() }}</ins>
                                        </span>
                                    </a>
                                </li>
                                @if ($post->username->id == Auth::user()->id)

                                <li>
                                    <a href="/post/{{$post->id}}/edit">
                                        <span class="comment" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="/post/{{ $post->id }}"
                                        onclick="event.preventDefault(); document.getElementById('submit-form').submit();">
                                        @method('delete')
                                        <span class=" comment" data-toggle="tooltip" title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </a>
                                    <form id="submit-form" action="/post/{{ $post->id }}" method="POST" class="hidden">
                                        @csrf

                                        @method('DELETE')
                                    </form>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="description">

                            <p>
                                {{ $post->konten }}
                            </p>
                        </div>
                    </div>

                    @endif
                </div>

                <div class=" coment-area">
                    <ul class="we-comet">
                        @foreach ($post->comments as $comment)

                        <li>
                            <div class="comet-avatar">
                                <img src="images/resources/comet-1.jpg" alt="">
                            </div>
                            <div class="we-comment">
                                <div class="coment-head">
                                    <h5>
                                        <a href="{{ url('/profils', $comment->username->id) }}"
                                            title="">{{ $comment->username->name }}</a>
                                    </h5>

                                    <span>{{ $comment->created_at->format('j F, Y') }}</span>

                                    <a href="{{ url('/like-comment', $comment->id) }}">
                                        <span class="like" data-toggle="tooltip" title="like">
                                            <i class="ti-heart"></i>
                                            <ins>{{ $comment->likeComments->count() }}</ins>
                                        </span>
                                    </a>

                                </div>
                                <p>
                                    {{ $comment->komentar }}
                                </p>

                            </div>
                        </li>

                        @endforeach
                        {{-- <li>
                            <a href="#" title="" class="showmore underline">more
                                comments</a>
                        </li> --}}
                        <li class="post-comment">
                            <div class="comet-avatar">
                                {{-- <img src="images/resources/comet-1.jpg" alt=""> --}}
                            </div>
                            <form method="post" action="{{ url('/comment', $post->id) }}">
                                @csrf
                                <div class="post-comt-box">
                                    <textarea class="form-control" placeholder="Post your comment"
                                        name="komentar"></textarea>
                                    {{-- <div class="add-smiles">
                                        <span class="em em-expressionless" title="add icon"></span>
                                    </div>
                                    <div class="smiles-bunch">
                                        <i class="em em---1"></i>
                                        <i class="em em-smiley"></i>
                                        <i class="em em-anguished"></i>
                                        <i class="em em-laughing"></i>
                                        <i class="em em-angry"></i>
                                        <i class="em em-astonished"></i>
                                        <i class="em em-blush"></i>
                                        <i class="em em-disappointed"></i>
                                        <i class="em em-worried"></i>
                                        <i class="em em-kissing_heart"></i>
                                        <i class="em em-rage"></i>
                                        <i class="em em-stuck_out_tongue"></i>
                                    </div> --}}
                                    <button class="mt-1" type="submit">Post</button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection
