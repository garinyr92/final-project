@extends('layouts.master')

@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="editing-info">
            <h5 class="f-title"><i class="ti-lock"></i>Change Password</h5>

            <form method="post" action="{{ url('/profil/save-password') }}">
                @csrf
                <div class="form-group">
                    <input type="password" required name="old_password" />
                    <label class="control-label" for="input">Current password</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group">
                    <input type="password" id="input" required name="new_password" />
                    <label class="control-label" for="input">New password</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group">
                    <input type="password" required name="new_password_confirmation" />
                    <label class="control-label" for="input">Confirm password</label><i class="mtrl-select"></i>
                </div>
                <div class="submit-btns">
                    <button type="submit" class="mtr-btn"><span>Update</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
