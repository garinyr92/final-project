@extends('layouts.master')


@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="editing-info">
            <h5 class="f-title"><i class="ti-info-alt"></i> Edit Basic Information</h5>

            <form method="post" action="{{ url('profil/save-info') }}">
                @csrf
                <div class="form-group">
                    <input type="text" id="input" required name="name" value="{{ Auth::user()->name }}" />
                    <label class="control-label" for="input">Nama</label><i class="mtrl-select"></i>
                </div>
                <div class="form-group">
                    <input type="email" required name="email" value="{{ Auth::user()->email }}" />
                    <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
                </div>

                @if(Auth::user()->profile != null)
                <div class="form-group">
                    <input type="date" required name="tgl_lahir" value="{{ Auth::user()->profile->tgl_lahir }}" />
                    <label class="control-label" for="input">Tanggal Lahir</label><i class="mtrl-select"></i>
                </div>
                <div class="form-radio">
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="Pria"
                                {{ (Auth::user()->profile->jk == 'Pria') ? 'checked': '' }}><i
                                class="check-box"></i>Pria
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="Wanita"
                                {{ (Auth::user()->profile->jk == 'Wanita') ? 'checked': '' }}><i
                                class="check-box"></i>Wanita
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <textarea rows="4" id="textarea" required
                        name="alamat">{{ Auth::user()->profile->alamat }}</textarea>
                    <label class="control-label" for="textarea">Alamat</label><i class="mtrl-select"></i>
                </div>
                @else
                <div class="form-group">
                    <input type="date" required name="tgl_lahir" />
                    <label class="control-label" for="input">Tanggal Lahir</label><i class="mtrl-select"></i>
                </div>
                <div class="form-radio">
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="Pria"><i class="check-box"></i>Pria
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="jk" value="Wanita"><i class="check-box"></i>Wanita
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <textarea rows="4" id="textarea" required name="alamat"></textarea>
                    <label class="control-label" for="textarea">Alamat</label><i class="mtrl-select"></i>
                </div>
                @endif
                <div class="submit-btns">
                    <button type="button" class="mtr-btn"><span>Cancel</span></button>
                    <button type="submit" class="mtr-btn"><span>Update</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
