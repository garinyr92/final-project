@extends('layouts.master')

@section('top-area')
<section>
    <div class="feature-photo">
        <figure>
            <img src="{{ asset('asset/images/resources/timeline-1.jpg') }}" alt="" />
        </figure>
        <div class="add-btn">
            @if ($friend != null)

            @if ($friend->id != Auth::user()->id)

            <span>{{ $friend->following->count() }} following</span>
            <span>{{ $friend->followers->count() }} followers</span>

            @if($user->following->count() > 0)
            @foreach ($user->following as $following)
            @if ($following->friend_id == $friend->id)
            <a href="{{ url('/friend/delete', $friend->id) }}" title="" data-ripple="">Remove Friend</a>
            @else
            <a href="{{ url('/friend/add', $friend->id) }}" title="" data-ripple="">Add Friend</a>
            @endif
            @endforeach
            @else
            <a href="{{ url('/friend/add', $friend->id) }}" title="" data-ripple="">Add Friend</a>
            @endif
            @endif

            @else

            <span>{{ $user->following->count() }} following</span>
            <span>{{ $user->followers->count() }} followers</span>

            @endif
        </div>
        <div class="container-fluid">
            <div class="row merged">
                <div class="col-lg-2 col-sm-3">
                    {{-- <div class="user-avatar">
                        <figure>
                            <img src="{{ asset('asset/images/resources/user-avatar.jpg') }}" alt="" />
                    <form class="edit-phto">
                        <i class="fa fa-camera-retro"></i>
                        <label class="fileContainer">
                            Edit Display Photo
                            <input type="file" />
                        </label>
                    </form>
                    </figure>
                </div> --}}
            </div>
            <div class="col-lg-10 col-sm-9">
                <div class="timeline-info">
                    <ul>
                        <li class="admin-name">
                            @if ($friend != null)

                            @if ($friend->id != Auth::user()->id)
                            <h5>{{ $friend->name }}</h5>
                            @endif

                            @else
                            <h5>{{ $user->name }}</h5>
                            @endif
                            {{-- <span>Group Admin</span> --}}
                        </li>
                        <li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('right-sidebar')
<div class="col-lg-3">
    <aside class="sidebar static">
        <div class="widget">
            <h4 class="widget-title">
                Profile intro
            </h4>
            <ul class="short-profile">
                @if ($user->profile == null)
                <li>
                    <span>Tanggal Lahir</span>
                    <p>
                        -
                    </p>
                </li>
                <li>
                    <span>Jenis Kelamin</span>
                    <p>
                        -
                    </p>
                </li>
                <li>
                    <span>Alamat</span>
                    <p>
                        -
                    </p>
                </li>
                @else
                <li>
                    <span>Tanggal Lahir</span>
                    <p>
                        {{ $user->profile->tgl_lahir }}
                    </p>
                </li>
                <li>
                    <span>Jenis Kelamin</span>
                    <p>
                        {{ $user->profile->jk }}
                    </p>
                </li>
                <li>
                    <span>Alamat</span>
                    <p>
                        {{ $user->profile->alamat }}
                    </p>
                </li>
                @endif
            </ul>
        </div>
    </aside>
</div>
<!-- profile intro widget -->
@endsection

@section('content')
<div class="col-lg-6">
    @if ($friend == null)
    <div class="central-meta">
        <div class="new-postbox">
            {{-- <figure>
                <img src="{{ asset('asset/images/resources/admin2.jpg') }}" alt="">
            </figure> --}}
            <div class="newpst-input">
                <form method="post" action="{{ url('post')}}" enctype="multipart/form-data">
                    @csrf
                    <textarea rows="2" placeholder="write something" name="konten"></textarea>
                    <div class="attachments">
                        <ul>
                            <li>
                                <i class="fa fa-image"></i>
                                <label class="fileContainer">
                                    <input type="file" name="image">
                                </label>
                            </li>
                            <li>
                                <button type="submit">Post</button>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- add post new box -->
    @endif
    <div class="loadMore">
        @foreach ($posts as $post)

        <div class="central-meta item">
            <div class="user-post">
                <div class="friend-info">
                    {{-- <figure>
                        <img src="{{ asset('asset/images/resources/friend-avatar10.jpg') }}" alt="">
                    </figure> --}}
                    <div class="friend-name">
                        <ins><a href="{{ url('/profils', $post->username->id) }}"
                                title="">{{ $post->username->name }}</a></ins>
                        <span>published: {{ $post->created_at->format('j F, Y') }}</span>
                    </div>
                    <div class="post-meta">
                        <img src="images/resources/user-post.jpg" alt="">
                        <div class="we-video-info">
                            <ul>
                                <li>
                                    <span class="comment" data-toggle="tooltip" title="Comments">
                                        <i class="fa fa-comments-o"></i>
                                        <ins>{{ $post->comments->count() }}</ins>
                                    </span>
                                </li>

                                <li>
                                    <a href="{{ url('/like-post', $post->id) }}">
                                        <span class="like" data-toggle="tooltip" title="like">
                                            <i class="ti-heart"></i>
                                            <ins>{{ $post->likes->count() }}</ins>
                                        </span>
                                    </a>
                                </li>

                                @if ($post->username->id == Auth::user()->id)

                                <li>
                                    <a href="/post/{{$post->id}}/edit">
                                        <span class="comment" data-toggle="tooltip" title="Edit">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </span>
                                    </a>
                                </li>

                                <li>
                                    <a href="/post/{{ $post->id }}"
                                        onclick="event.preventDefault(); document.getElementById('submit-form').submit();">
                                        @method('delete')
                                        <span class=" comment" data-toggle="tooltip" title="Delete">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </a>
                                    <form id="submit-form" action="/post/{{ $post->id }}" method="POST" class="hidden">
                                        @csrf

                                        @method('DELETE')
                                    </form>
                                </li>

                                @endif

                            </ul>
                        </div>
                        <div class="description">

                            <p>
                                {{ $post->konten }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="coment-area">
                    <ul class="we-comet">
                        @foreach ($post->comments as $comment)

                        <li>
                            <div class="comet-avatar">
                                <img src="images/resources/comet-1.jpg" alt="">
                            </div>
                            <div class="we-comment">
                                <div class="coment-head">
                                    <h5>
                                        <a href="{{ url('/profils', $comment->username->id) }}"
                                            title="">{{ $comment->username->name }}</a>
                                    </h5>

                                    <span>{{ $comment->created_at->format('j F, Y') }}</span>

                                    <a href="{{ url('/like-comment', $comment->id) }}">
                                        <span class="like" data-toggle="tooltip" title="like">
                                            <i class="ti-heart"></i>
                                            <ins>{{ $comment->likeComments->count() }}</ins>
                                        </span>
                                    </a>

                                </div>
                                <p>
                                    {{ $comment->komentar }}
                                </p>

                            </div>
                        </li>

                        @endforeach
                        {{-- <li>
                            <a href="#" title="" class="showmore underline">more
                                comments</a>
                        </li> --}}
                        <li class="post-comment">
                            <div class="comet-avatar">
                                {{-- <img src="images/resources/comet-1.jpg" alt=""> --}}
                            </div>
                            <form method="post" action="{{ url('/comment', $post->id) }}">
                                @csrf
                                <div class="post-comt-box">
                                    <textarea class="form-control" placeholder="Post your comment"
                                        name="komentar"></textarea>
                                    {{-- <div class="add-smiles">
                                        <span class="em em-expressionless" title="add icon"></span>
                                    </div>
                                    <div class="smiles-bunch">
                                        <i class="em em---1"></i>
                                        <i class="em em-smiley"></i>
                                        <i class="em em-anguished"></i>
                                        <i class="em em-laughing"></i>
                                        <i class="em em-angry"></i>
                                        <i class="em em-astonished"></i>
                                        <i class="em em-blush"></i>
                                        <i class="em em-disappointed"></i>
                                        <i class="em em-worried"></i>
                                        <i class="em em-kissing_heart"></i>
                                        <i class="em em-rage"></i>
                                        <i class="em em-stuck_out_tongue"></i>
                                    </div> --}}
                                    <button class="mt-1" type="submit">Post</button>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection
