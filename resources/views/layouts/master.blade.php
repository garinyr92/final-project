<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>Winku Social Network Toolkit</title>
    <link rel="icon" href="{{ asset('asset/images/fav.png') }}" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="{{ asset('asset/css/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/responsive.css') }}">
    @stack('css')

</head>

<body>
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    <!--<div class="se-pre-con"></div>-->
    <div class="theme-layout">

        <!-- responsive header -->
        @include('layouts.partials.responsive-header')
        <!-- responsive header -->

        <!-- topbar -->
        @include('layouts.partials.topbar')
        <!-- topbar -->

        <!-- top area -->
        @yield('top-area')
        <!-- top area -->

        <section>
            <div class="gap2 gray-bg">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <!-- left sidebar -->
                                @include('layouts.partials.left-sidebar')
                                <!-- left sidebar -->

                                {{-- content --}}
                                @yield('content')
                                {{-- end content --}}

                                <!-- right sidebar -->
                                @yield('right-sidebar')
                                <!-- right sidebar -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="bottombar">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="copyright"><a target="_blank" href="https://www.templateshub.net">Templates
                                Hub</a></span>
                        <i><img src="images/credit-cards.png" alt=""></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> --}}
    <script src="{{ asset('asset/js/main.min.js') }}"></script>
    <script src="{{ asset('asset/js/script.js') }}"></script>
    {{-- <script src="{{ ('asset/js/map-init.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8c55_YHLvDHGACkQscgbGLtLRdxBDCfI"></script> --}}
    @stack('script')

</body>

</html>
