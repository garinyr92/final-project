<div class="col-lg-3">
    <aside class="sidebar static right">
        <div class="widget stick-widget">
            <h4 class="widget-title">Account Setting</h4>
            <ul class="naves">
                <li>
                    <i class="ti-info-alt"></i>
                    <a href="{{ url('/profil/info') }}" title="">Basic info</a>
                </li>
                <li>
                    <i class="ti-lock"></i>
                    <a href="{{ url('/profil/edit-password') }}" title="">change password</a>
                </li>
            </ul>
        </div>
    </aside>
</div>
