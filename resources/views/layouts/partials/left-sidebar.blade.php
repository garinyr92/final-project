<div class="col-lg-3">
    <aside class="sidebar static left">
        <div class="widget">
            <h5 class="widget-title">Welcome, {{ AUth::user()->name }}</h5>
            <h4 class="widget-title">Menu</h4>
            <ul class="naves">
                <li>
                    <i class="ti-clipboard"></i>
                    <a href="{{ url('/') }}" title="">Home</a>
                </li>
                <li>
                    <i class="ti-id-badge"></i>
                    <a href="{{ url('/profil') }}" title="">My Profil</a>
                </li>
                <li>
                    <i class="ti-user"></i>
                    <a href="{{ url('/friends') }}" title="">friends</a>
                </li>
                <li>
                    <i class="ti-power-off"></i>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </div><!-- Shortcuts -->
        <div class="widget">
            <h4 class="widget-title">Account Setting</h4>
            <ul class="naves">
                <li>
                    <i class="ti-info-alt"></i>
                    <a href="{{ url('/profil/info') }}" title="">Basic info</a>
                </li>
                <li>
                    <i class="ti-lock"></i>
                    <a href="{{ url('/profil/edit-password') }}" title="">change password</a>
                </li>
            </ul>
        </div><!-- Shortcuts -->
    </aside>
</div>
