<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>Winku Social Network Toolkit</title>
    <link rel="icon" href="{{ asset('asset/images/fav.png') }}" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="{{ asset('asset/css/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/color.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/responsive.css') }}">

</head>

<body>
    <div class="theme-layout">
        <div class="container-fluid pdng0">
            <div class="row merged">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="land-featurearea">
                        <div class="land-meta">
                            <div class="mb-2">
                                <span><img src="{{ asset('asset/images/logo2.png') }}" alt="" /></span>
                            </div>
                            <p>
                                Sosial Media kita bersama.
                            </p>
                            <div class="friend-logo">
                                <span><img src="{{ asset('asset/images/wink.png') }}" alt="" /></span>
                            </div>
                            {{-- <a href="#" title="" class="folow-me">Follow Us on</a> --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    {{-- <div class="login-reg-bg"> --}}
                    @yield('content-page')
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>

    {{-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> --}}
    <script src="{{ asset('asset/js/main.min.js') }}"></script>
    <script src="{{ asset('asset/js/script.js') }}"></script>
    {{-- @stack('script') --}}
    {{-- <script src="{{ ('asset/js/map-init.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8c55_YHLvDHGACkQscgbGLtLRdxBDCfI"></script> --}}

</body>

</html>
