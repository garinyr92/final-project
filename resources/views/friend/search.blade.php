@extends('layouts.master')

@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="frnds">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="active" href="#frends" data-toggle="tab">Users</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                {{-- following --}}
                <div class="tab-pane active fade show " id="frends">
                    <ul class="nearby-contct">
                        @foreach ($users as $user)

                        @if($user->id != Auth::user()->id)
                        <li>
                            <div class="nearly-pepls">
                                <figure>
                                </figure>
                                <div class="pepl-info">
                                    <h4>
                                        <a href="{{ url('/profils', $user->id ) }}" title="">{{ $user->name }}</a>
                                    </h4>
                                    <a href="{{ url('/profils', $user->id) }}" title="" class="add-butn"
                                        data-ripple="">Profile</a>
                                </div>
                            </div>
                        </li>
                        @endif

                        @endforeach
                    </ul>
                    <div class="lodmore">
                        <button class="btn-view btn-load-more"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
