@extends('layouts.master')

@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="frnds">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="active" href="#frends" data-toggle="tab">Following</a>
                    <span>{{ $user->following->count() }}</span>
                </li>
                <li class="nav-item">
                    <a class="" href="#frends-req"
                        data-toggle="tab">Follower</a><span>{{ $user->followers->count() }}</span>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                {{-- following --}}
                <div class="tab-pane active fade show " id="frends">
                    <ul class="nearby-contct">
                        @foreach ($user->following as $following)

                        <li>
                            <div class="nearly-pepls">
                                <figure>
                                    {{-- <a href="time-line.html" title=""><img src="images/resources/friend-avatar9.jpg"
                                            alt="" /></a> --}}
                                </figure>
                                <div class="pepl-info">
                                    <h4>
                                        <a href="{{ url('/profils', $following->followingName->id ) }}"
                                            title="">{{ $following->followingName->name }}</a>
                                    </h4>
                                    {{-- <span>ftv
                                        model</span> --}}
                                    <a href="{{ url('/friend/delete', $following->followingName->id) }}" title=""
                                        class="add-butn more-action" data-ripple="">unfriend</a>
                                    {{-- <a href="#" title="" class="add-butn" data-ripple="">add
                                        friend</a> --}}
                                </div>
                            </div>
                        </li>

                        @endforeach

                    </ul>
                    <div class="lodmore">
                        <button class="btn-view btn-load-more"></button>
                    </div>
                </div>
                {{-- followers --}}
                <div class="tab-pane fade" id="frends-req">
                    <ul class="nearby-contct">

                        @foreach ($user->followers as $followers)
                        <li>
                            <div class="nearly-pepls">
                                <figure>
                                    {{-- <a href="time-line.html" title=""><img src="images/resources/nearly5.jpg"
                                            alt="" /></a> --}}
                                </figure>
                                <div class="pepl-info">
                                    <h4>
                                        <a href="{{ url('/profils', $followers->followerName->id) }}"
                                            title="">{{ $followers->followerName->name }}</a>
                                    </h4>
                                    {{-- <span>ftv
                                        model</span> --}}
                                    {{-- <a href="#" title="" class="add-butn more-action" data-ripple="">delete
                                        Request</a> --}}
                                    <a href="{{ url('/friend/add', $followers->followerName->id) }}" title=""
                                        class="add-butn" data-ripple="">Add Friend</a>
                                </div>
                            </div>
                        </li>
                        @endforeach

                    </ul>
                    <button class="btn-view btn-load-more"></button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
