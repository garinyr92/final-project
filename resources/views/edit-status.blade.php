@extends('layouts.master')

@section('content')
<div class="col-lg-6">
    <div class="central-meta">
        <div class="new-postbox">
            {{-- <figure>
                <img src="{{ asset('asset/images/resources/admin2.jpg') }}" alt="">
            </figure> --}}
            <div class="newpst-input">
                <form method="post" action="{{ url('post', $post->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <textarea rows="2" placeholder="write something" name="konten">{{ $post->konten }}</textarea>
                    <div class="attachments">
                        <ul>
                            <li>
                                <i class="fa fa-image"></i>
                                <label class="fileContainer">
                                    <input type="file" name="image">
                                </label>
                            </li>
                            <li>
                                <button type="submit">Update</button>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- add post new box -->
</div>
@endsection
