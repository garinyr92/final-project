@extends('layouts.master-login')

@section('content-page')
<div class="log-reg-area sign">
    <h2 class="log-title">Register</h2>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
            <input type="text" required="required" name="name" value="{{ old('name') }}" />
            <label class="control-label" for="input">User Name</label><i class="mtrl-select"></i>
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="email" required="required" name="email" value="{{ old('email') }}" />
            <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password" required="required" name="password" />
            <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input type="password" required="required" id="password-confirm" name="password_confirmation" />
            <label class="control-label" for="input">Confirm Password</label><i class="mtrl-select"></i>
            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <a href="{{ route('login') }}" title="" class="already-have">Already have an account</a>
        <div class="submit-btns">
            <button class="mtr-btn signup" type="submit">
                <span>Register</span>
            </button>
        </div>
    </form>
</div>
@endsection

@push('script')

<script src="{{ asset('js/app.js') }}" defer></script>
@endpush
