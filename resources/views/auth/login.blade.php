@extends('layouts.master-login')

@section('content-page')
<div class="log-reg-area sign">
    <h2 class="log-title">Login</h2>
    <form method="post" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <input type="email" id="email" required="required" name="email" />
            <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
        </div>
        <div class="form-group">
            <input type="password" required="required" name="password" />
            <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
        </div>
        {{-- <a href="#" title="" class="forgot-pwd">Forgot Password?</a> --}}
        <div class="submit-btns">
            <button class="mtr-btn signin" type="submit">
                <span>Login</span>
            </button>
            <a href="{{ route('register') }}" class="mtr-btn signup">
                <span>Register</span>
            </a>
            {{-- <button class="mtr-btn signup" type="button">
                <span>Register</span>
            </button> --}}
        </div>
    </form>
</div>
@endsection
