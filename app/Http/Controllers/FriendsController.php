<?php

namespace App\Http\Controllers;

use App\friends;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;

use Auth;
use App\User;
use App\Profil;
use App\Post;
use App\Friend;

class FriendsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = User::find(Auth::user()->id);
        // dd($user->name);
        return view('friend.friend', compact('user'));
    }

    public function add_friend($id){
        $friend = User::find($id);

        // dd($friend->id == Auth::user()->id);
        if($friend->id != Auth::user()->id){
            $query = DB::table('friends')->insert([
                "user_id" => Auth::user()->id,
                "friend_id" => $friend->id,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($query){
                Alert::success('Success', 'Berhasil Tambah Teman');
                return redirect()->back();
            }else{
                Alert::error('Error', 'Gagal Tambah Teman');
                return redirect()->back();
            }
        }
    }

    public function delete_friend($id){
        // dd($id);

        $query = Friend::where('friend_id', $id)
        ->where('user_id', Auth::user()->id)
        ->first();

        // dd($query);
        if($query){
            $delete = $query->delete();

            if($delete){
                Alert::success('Success', 'Berhasil Tambah Teman');
                return redirect()->back();
            }else{
                Alert::error('Error', 'Gagal Tambah Teman');
                return redirect()->back();
            }
        }else{
            Alert::error('Error', 'Tidak Ditemukan');
            return redirect()->back();
        }

    }

    public function search_friend(Request $request)
    {
        // dd($request->all());
        $users = DB::table('users')
        ->where('name', 'like', '%' . $request->q . '%')
        ->get();
        // dd($users);
        return view('friend.search', compact('users'));
    }
}
