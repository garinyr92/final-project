<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Profil;
use App\Post;
use App\Friend;

class ProfilController extends Controller
{

    public function index()
    {
        $user = User::find(Auth::user()->id);
        $friend = null;

        $posts = Post::where('user_id', Auth::user()->id)
        ->orderby('created_at', 'desc')
        ->get();

        // dd($posts->comments);
        return view('profil.index', compact('posts', 'user', 'friend'));
    }

    public function friend_index($id)
    {
        // dd($id);
        // $id = 1;
        $friend = User::find($id);
        $user = User::find(Auth::user()->id);
        $posts = Post::where('user_id', $id)
        ->orderby('created_at', 'desc')
        ->get();

        // dd($posts);
        // dd($follow->id == Auth::user()->id);
        // foreach($friend->following as $following){
        //     dd($following->friend_id == Auth::user()->id);
        // }

        return view('profil.index', compact('posts', 'user', 'friend'));
    }

    public function info()
    {
        return view('profil.basic-info');
    }

    public function edit_password(Request $request)
    {
        return view('profil.edit-password');
    }
    public function save_password(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if($request->new_password == $request->new_password_confirmation){
            if(Hash::check($request->old_password, $user->password)){
                $user->password = Hash::make($request->new_password);
                $user->save();

                Alert::success('Success', 'Berhasil Mengganti Password');
                return redirect('profil/edit-password');
            }else{
                Alert::error('Error', 'Password Lama Salah !');
                return redirect('profil/edit-password');
            }
        }else{
            Alert::error('Error', 'Password konfirmasi tidak sama !');
            return redirect('profil/edit-password');
        }
    }

    public function save_info(Request $request){

        // dd($request->all());

        $user = User::find(Auth::user()->id);

        if($request->name != $user->name){
            $user->name = $request->name;
        }
        if($request->email != $user->email){
            $user->email = $request->email;
        }
        $user->save();

        $profil = Profil::where('user_id', $user->id)->first();
        // dd($profil);

        if($profil == null){
            $query = DB::table('profils')->insert([
                "tgl_lahir" => $request["tgl_lahir"],
                "jk" => $request["jk"],
                "alamat" => $request["alamat"],
                "user_id" => Auth::user()->id,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($query){
                Alert::success('Success', 'Berhasil Update Profil');
                return redirect('profil/info');
            }else{
                Alert::error('Error', 'Gagal Update Profil');
                return redirect('profil/info');
            }
        }else{
            $query = DB::table('profils')->update([
                "tgl_lahir" => $request["tgl_lahir"],
                "jk" => $request["jk"],
                "alamat" => $request["alamat"],
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($query){
                Alert::success('Success', 'Berhasil Update Profil');
                return redirect('profil/info');
            }else{
                Alert::error('Error', 'Gagal Update Profil');
                return redirect('profil/info');
            }
        }
    }
}
