<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

use Auth;
use App\User;
Use App\Post;
Use App\LikePost;
Use App\LikeKomen;

class LikeController extends Controller
{
    public function like($id)
    {
        $like = LikePost::where('post_id', $id)
        ->where('user_id', Auth::user()->id)
        ->first();

        if($like == null){
            $query = DB::table('like_posts')->insert([
                'post_id' => $id,
                'user_id' => Auth::user()->id,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            if($query){
                return redirect('index');
            }
        }else{
            $query = $like->delete();

            if($query){
                return redirect('index');
            }
        }

    }

    public function dislike($id)
    {
        // dd($id);
        $like = LikePost::where('post_id', $id)
        ->where('user_id', Auth::user()->id)
        ->first();
        $query = $like->delete();

        if($query){
            return redirect('index');
        }

    }

    public function likeComment($id)
    {
        $likeComment = LikeKomen::where('komentar_id', $id)
        ->where('user_id', Auth::user()->id)
        ->first();

        // dd($likeComment);
        if($likeComment == null){
            $query = DB::table('like_komens')->insert([
                'komentar_id' => $id,
                'user_id' => Auth::user()->id,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($query){
                return redirect('index');
            }
        }else{
            $query = $likeComment->delete();

            if($query){
                return redirect('index');
            }
        }


    }

    public function dislikeComment($id)
    {
        // dd($id);
        $like = LikeKomen::where('komentar_id', $id)
        ->where('user_id', Auth::user()->id)
        ->first();
        $query = $like->delete();

        if($query){
            return redirect('index');
        }

    }
}
