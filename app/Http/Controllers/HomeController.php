<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

use Auth;
use App\User;
Use App\Post;
Use App\Friend;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        // $user = User::find(Auth::user()->id);

        $friends = Friend::where('user_id', Auth::user()->id)->get();
        // dd($friends);
        // ->get();
        if($friends->count() > 0){
            $arr= [];
            foreach($friends as $friend){
                array_push($arr, $friend->friend_id);
            }
            // dd($arr);
        }
        $posts = Post::
        where('user_id', Auth::user()->id)
        ->orderby('created_at', 'desc')
        ->get();

        // dd($friends);

        // $postFriends = Post::where('user_id', $friends->friend_id)
        // ->orderby('created_at', 'desc')
        // ->get();


        // $postFriends = DB::table('posts')
        //     ->join('users', 'users.id', '=', 'posts.user_id')
        //     ->join('friends', 'users.id', '=', 'friends.user_id')
        //     ->orderby('posts.created_at', 'desc')
        //     ->get();

        //     dd($postFriends);
        // $konten = Post::all();

        // dd($posts->comments);
        // dd($posts->username);
        return view('index', compact('posts', 'friends'));
    }
}
