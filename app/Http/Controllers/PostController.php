<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

use Auth;
use App\User;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if($request->hasFile('image')){
            $request->validate([
                'konten' => 'required',
            ]);

            $nama = ucfirst(Auth::user()->name);
            $file_image = $request->file('image');
            $filename_image = time() .'_' . $nama .'.' . $file_image->getClientOriginalExtension();
            $file_image->storeAs('public/post/', $filename_image);

            $query = DB::table('posts')->insert([
                'user_id' => Auth::user()->id,
                "konten" => $request["konten"],
                "image" => $filename_image,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            return redirect('index');

        }else{
            $post = Post::create([
                "konten" => $request->konten,
                "user_id" => Auth::user()->id,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            return redirect('index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        // dd($post);

        return view('edit-status', compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->hasFile('image')){
            $request->validate([
                'konten' => 'required',
            ]);

            // dd($post);

            $nama = ucfirst(Auth::user()->name);
            $file_image = $request->file('image');
            $filename_image = time() .'_' . $nama .'.' . $file_image->getClientOriginalExtension();
            $file_image->storeAs('public/post/', $filename_image);

            $post = Post::find($id);

            $post->update([
                'user_id' => Auth::user()->id,
                "konten" => $request["konten"],
                "image" => $filename_image,
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($post){
                // Alert::success('Success', 'Berhasil posting');
                return redirect('index');
            }else{
                // Alert::error('Error', 'Gagal posting');
                return redirect('index');
            }
        }else{
            $request->validate([
                'konten' => 'required',
            ]);

            $post = Post::find($id);

            $post->update([
                'user_id' => Auth::user()->id,
                "konten" => $request["konten"],
                "created_at" =>  date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if($post){
                // Alert::success('Success', 'Berhasil posting');
                return redirect('index');
            }else{
                // Alert::error('Error', 'Gagal posting');
                return redirect('index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        // dd($post);
        if($post->user_id == Auth::user()->id){
            $post->delete();
            Alert::success('Success', 'Berhasil hapus posting');
            return redirect('index');
        }else{
            Alert::error('Error', 'Error hapus posting');
            return redirect('index');
        }

    }
}
