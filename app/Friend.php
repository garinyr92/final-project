<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $fillable = [
        'user_id', 'friend_id',
    ];

    protected $table = "friends";

    public function followingName()
    {
        return $this->belongsTo('App\User', 'friend_id');
    }

    public function followerName()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
