<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'konten', 'image',
    ];

    protected $table = "posts";

    public function username()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Komentar', 'post_id');
    }

    public function likes()
    {
        return $this->hasMany('App\LikePost', 'post_id');
    }
}
