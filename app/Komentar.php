<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $fillable = [
        'post_id', 'user_id', 'komentar',
    ];

    protected $table = "komentars";

    public function username()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function likeComments()
    {
        return $this->hasMany('App\LikeKomen', 'komentar_id');
    }
}
