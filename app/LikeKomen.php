<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeKomen extends Model
{
    protected $fillable = [
        'komentar_id', 'user_id',
    ];

    protected $table = "like_komens";

    public function username()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function like()
    {
        return $this->belongsTo('App\Komentar', 'id', 'komentar_id');
    }
}
