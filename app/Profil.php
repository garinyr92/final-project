<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $fillable = [
        'user_id', 'tgl_lahir', 'jk', 'alamat'
    ];

    protected $table = "profils";
}
