<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePost extends Model
{
    protected $fillable = [
        'post_id', 'user_id'
    ];

    protected $table = "like_posts";

    public function username()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    // public function likePost()
    // {
    //     return $this->belongsTo('App\Post', 'post_id');
    // }

    // public function comments()
    // {
    //     return $this->hasMany('App\Komentar', 'post_id');
    // }
}
